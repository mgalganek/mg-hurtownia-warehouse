<!DOCTYPE html>

<%inherit file="base.mako"/>

<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"> Historia Zamówień </h1>
        <ol class="breadcrumb">
            <li class="active">
                <i class="fa fa-dashboard"></i> <a href="${request.application_url}"> Strona główna </a>
            </li>
            <li class="active">
                <i class="fa fa-bar-chart-o"></i> Historia Zamówień
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->

% if orders:
<div class="col-lg-12">
    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Status</th>
                    <th>Data złożenia</th>
                    <th>Zamówienia</th>
                    <th>Dostawa</th>
                    <th>Płatność</th>
                 </tr>
            </thead>
            <tbody>
                % for order in orders:
                    <tr>
                        <td>${order[0].Status_str}</td>
                        <td>${order[0].DateIssue}</td>
                        <td>
                            Zamówienie nr
                            <a href=order_details/${order[0].Id}>
                                ${order[0].Id}
                            </a>
                        </td>
                        <td>
                        % if order[2]:
                            ${order[2].Method}
                        % else:
                            Brak
                        % endif
                        </td>
                        <td>
                        % if order[3]:
                            ${order[3].Method}
                        % else:
                            Brak
                        % endif
                        </td>
                    </tr>
                % endfor
            </tbody>
        </table>
    </div>

% else:
    Brak zamówień

% endif
</div>

<div class="col-lg-12">
    % if orders.pager():
        <div class="breadcrumb">
        ${orders.pager()}
    </div>
    % endif
</div>

<!DOCTYPE html>

<%inherit file="base.mako"/>

<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"> Twoje dane </h1>
        <ol class="breadcrumb">
            <li class="active">
                <i class="fa fa-dashboard"></i> <a href="${request.application_url}"> Strona główna </a>
            </li>
            <li class="active">
                <i class="fa fa-bar-chart-o"></i> Twoje dane
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="jumbotron">
            <ul>
                <li>
                    <p>Imię i nazwisko: ${user_object.Name}</p>
                </li>
                <li>
                    <p>Pesel: ${user_object.Pesel}</p>
                </li>
                <li>
                    <p>NIP: ${user_object.Nip}</p>
                </li>
                <li>
                    <p>Regon: ${user_object.Regon}</p>
                </li>
                <li>
                    <p>Email: ${user_object.Email}</p>
                </li>
            </ul>
        </div>
    </div>
</div>

<!DOCTYPE html>

<%inherit file="base.mako"/>

<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"> Zamówienia oczekujące </h1>
        <ol class="breadcrumb">
            <li class="active">
                <i class="fa fa-dashboard"></i> <a href="${request.application_url}"> Strona główna </a>
            </li>
            <li class="active">
                <i class="fa fa-bar-chart-o"></i> Zamówienia oczekujące
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->

<div class="col-lg-8">
    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Numer</th>
                    <th>Data</th>
                    <th>Kwota</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                % for order in order_list:
                    <tr>
                        <td>
                            <a href=order_details/${order['number']}>${order['number']}</a>
                        </td>
                        <td>${order['date']}</td>
                        <td>${order['amount']}</td>
                        <td>
                            <a href="${request.application_url}/${order['edit_url']}">Edytuj</a>
                        </td>
                        <td>
                            <a href="${request.application_url}/${order['cancel_url']}">Anuluj</a>
                        </td>
                    </tr>
                % endfor
            </tbody>
        </table>
    </div>
</div>

<div class="col-lg-12">
    % if order_list.pager():
        <div class="breadcrumb">
        ${order_list.pager()}
    </div>
    % endif
</div>

<!DOCTYPE html>

<%inherit file="base.mako"/>

<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"> Strona główna </h1>
        <ol class="breadcrumb">
            <li class="active">
                <i class="fa fa-dashboard"></i> Strona główna
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-6 col-md-6">
        <div class="panel panel-green">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-tasks fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">${count_done}</div>
                        <div> Zrealizowanych zamówień </div>
                    </div>
                </div>
            </div>
            <a href="${request.application_url}/order_history">
                <div class="panel-footer">
                    <span class="pull-left"> Szczegóły </span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-6 col-md-6">
        <div class="panel panel-yellow">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-shopping-cart fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">${count_new}</div>
                        <div> Nowych zamówień </div>
                    </div>
                </div>
            </div>
            <a href="${request.application_url}/order_list">
                <div class="panel-footer">
                    <span class="pull-left"> Szczegóły </span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-long-arrow-right fa-fw"></i> Zamówienia </h3>
            </div>
            <div class="panel-body">
                <div id="morris-donut-chart"></div>
                <div class="text-right">
                    <a href="${request.application_url}/order_history"> Szczegóły <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(function() {
        Morris.Donut({
            element: 'morris-donut-chart',
            data: [{
                label: "Nowe",
                value: ${count_new},
            }, {
                label: "Anulowane",
                value: ${count_canceled},
            }, {
                label: "Zrealizowane",
                value: ${count_done},
            }],
            resize: true
        });
    });

</script>

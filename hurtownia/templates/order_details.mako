<!DOCTYPE html>

<%inherit file="base.mako"/>

<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"> Szczegóły zamówienia </h1>
        <ol class="breadcrumb">
            <li class="active">
                <i class="fa fa-dashboard">
                </i>
                <a href="${request.application_url}">
                    Strona główna
                </a>
            </li>
            <li class="active">
                <i class="fa fa-bar-chart-o">
                </i>
                <a href="${request.application_url}/order_history">
                    Historia Zamówień
                </a>
            </li>
            <li class="active">
                <i class="fa fa-bar-chart-o">
                    Szczegóły zamówienia nr ${order[0].Id}
                </i>
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->

<div class="col-lg-8">
    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Status</th>
                    <th>Data złożenia</th>
                    <th>Dostawa</th>
                    <th>Płatność</th>
                    <th>Kwota</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>${order[0].Status_str}</td>
                    <td>${order[0].DateIssue}</td>
                    <td>${order[2].Method}</td>
                    <td>${order[3].Method}</td>
                    <td>${amount}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="col-lg-8">
    <div class="table-responsive">
        <table class="table table-hover">
            % if order[1]:
                <thead>
                    <tr>
                        <th>Adres</th>
                        <th>Dane</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>Ulica</th>
                        <td>${order[1].Street}</td>
                    </tr>
                    <tr>
                        <th>Dom</th>
                        <td>${order[1].HouseNumber}</td>
                    </tr>
                    <tr>
                        <th>Mieszkanie</th>
                        <td>${order[1].FlatNumber}</td>
                    </tr>
                    <tr>
                        <th>Kod pocztowy</th>
                        <td>${order[1].PostalCode}</td>
                    </tr>
                    <tr>
                        <th>Miasto</th>
                        <td>${order[1].City}</td>
                    </tr>
                    <tr>
                        <th>Kraj</th>
                        <td>${order[1].Country}</td>
                    </tr>
                </tbody>
            % else:
                <thead>
                    <tr>
                        <th>Adres</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>Brak dodanego adresu</th>
                    </tr>
                </tbody>
            % endif
        </table>
    </div>
</div>

% if products:
<div class="col-lg-8">
    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Status</th>
                    <th>Nazwa</th>
                    <th>Cena</th>
                    <th>Ilość</th>
                 </tr>
            </thead>
            <tbody>
                % for product in products:
                    <tr>
                        <td>${product[0].Status_str}</td>
                        <td><a href=/warehouse/${product[0].Id}>${product[0].Name}</a></td>
                        <td>${product[0].Price * product[1]} PLN</td>
                        <td>${product[1]}</td>
                    </tr>
                % endfor
            </tbody>
        </table>
    </div>

% else:
    Brak produktów

% endif
</div>

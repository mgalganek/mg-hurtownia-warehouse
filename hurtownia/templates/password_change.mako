<!DOCTYPE html>

<%inherit file="base.mako"/>

<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"> Strona główna </h1>
        <ol class="breadcrumb">
            <li class="active">
                <i class="fa fa-dashboard"></i> <a href="${request.application_url}"> Strona główna </a>
            </li>
            <li class="active">
                <i class="fa fa-fw fa-gear"></i> Zmiana hasła 
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->

% if not success:
    % if message:
        <div class="alert alert-danger">
            <strong> ${message} </strong>
        </div>
    % endif
    <form role="form" method="post" action="${request.application_url}/password_change">
        <div class="col-lg-3">
            <div class="form-group">
                <label> Stare hasło: </label>
                <input type="password" class="form-control required" name="old_pswd">
            </div>
            <div class="form-group">
                <label> Nowe hasło: </label>
                <input type="password" class="form-control required" name="new_pswd">
            </div>
            <div class="form-group">
                <label> Powtórz nowe hasło: </label>
                <input type="password" class="form-control required" name="repeat_new_pswd">
            </div>
            <button type="submit" class="btn btn-success"> Zmień hasło </button>
            <button type="reset" class="btn btn-danger"> Wyczyść </button>
        </div>
    </div>
% else:
    <div class="alert alert-success">
        <strong>${message}</strong>
    </div>
    <a href="${request.application_url}">
        <button class="btn btn-success"> OK </button>
    </a>
% endif

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Hurtownia WEB</title>

    <!-- Bootstrap Core CSS -->
    <link href="/static/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="/static/css/sb-admin.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="/static/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Morris Charts CSS -->
    <link href="/static/css/plugins/morris.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- jQuery Version 1.11.0 -->
    <script src="/static/js/jquery-1.11.0.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="/static/js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="/static/js/plugins/morris/raphael.min.js"></script>
    <script src="/static/js/plugins/morris/morris.min.js"></script>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="${request.application_url}"> Hurtownia WEB </a>
            </div>

            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                % if user:
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> ${user} <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="${request.application_url}/user_details"><i class="fa fa-fw fa-user"></i> Profil </a>
                            </li>
                            <li>
                                <a href="${request.application_url}/password_change"><i class="fa fa-fw fa-gear"></i> Zmiana hasła </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="${request.application_url}/logout"><i class="fa fa-fw fa-power-off"></i> Wyloguj </a>
                            </li>
                        </ul>
                    </li>
                % else:
                    <li>
                        <a href="${request.application_url}/login" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Zaloguj </a>
                    </li>
                % endif
            </ul>

            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <a href="${request.application_url}"><i class="fa fa-fw fa-dashboard"></i> Strona główna </a>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#orders_collapsable"><i class="fa fa-fw fa-arrows-v"></i> Zamówienia <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="orders_collapsable" class="collapse">
                            <li>
                                <a href="${request.application_url}/order_new"> Nowe </a>
                            </li>
                            <li>
                                <a href="${request.application_url}/order_list"> Lista oczekujących </a>
                            </li>
                            <li>
                                <a href="${request.application_url}/order_history"> Historia </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="${request.application_url}/warehouse"><i class="fa fa-fw fa-bar-chart-o"></i> Przegląd magazynu</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->

        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page -->
                ${next.body()}

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

</body>

</html>

<!DOCTYPE html>

<%inherit file="base.mako"/>

<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"> Przegląd Magazynu </h1>
        <ol class="breadcrumb">
            <li class="active">
                <i class="fa fa-dashboard"></i> <a href="${request.application_url}"> Strona główna </a>
            </li>
            <li class="active">
                <i class="fa fa-bar-chart-o"></i> Przegląd Magazynu
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->

% if products:
<div class="col-lg-8">
    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Status</th>
                    <th>Nazwa</th>
                    <th>Cena</th>
                    <th>Ilość</th>
                 </tr>
            </thead>
            <tbody>
                % for product in products:
                    <tr>
                        <td>${product.Status_str}</td>
                        <td><a href=/warehouse/${product.Id}>${product.Name}</a></td>
                        <td>${product.Price}</td>
                        <td>${product.Quantity}</td>
                    </tr>
                % endfor
            </tbody>
        </table>
    </div>

% else:
    Brak produktów

% endif
</div>

<div class="col-lg-12">
    % if products.pager():
        <div class="breadcrumb">
        ${products.pager()}
    </div>
    % endif
</div>

<!DOCTYPE html>

<%inherit file="base.mako"/>

<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"> Anulowanie zamówienia </h1>
        <ol class="breadcrumb">
            <li class="active">
                <i class="fa fa-dashboard"></i> <a href="${request.application_url}"> Strona główna </a>
            </li>
            <li class="active">
                <i class="fa fa-bar-chart-o"></i><a href="${request.application_url}/order_list"> Zamówienia oczekujące </a>
            </li>
            <li class="active">
                <i class="fa fa-bar-chart-o"></i> Anulowanie zamówienia
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->

% if not confirmed:
    <div class="jumbotron">
        <form role="form" method="post" action="${request.url}">
            <h3> Czy napewno chcesz anulować zamówienie numer ${order_id}? </h3>
            <button type="submit" name="cancel_order" value="cancel_order" class="btn btn-success"> Anuluj zamówienie </button>
            <button type="submit" name="return" value="return" class="btn btn-danger"> Wróć </button>
        </form>
    </div>
% else:
    % if cancel_success:
        <div class="alert alert-success">
            Zamówienie numer ${order_id} zostało anulowane.
        </div>
    % else:
        <div class="alert alert-danger">
            Zamówienie numer ${order_id} nie zostało anulowane. Proszę spróbować jeszcze raz.
        </div>
    % endif
% endif

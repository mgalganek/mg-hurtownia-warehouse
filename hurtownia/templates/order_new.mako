<!DOCTYPE html>

<%inherit file="base.mako"/>

<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"> Nowe zamówienie </h1>
        <ol class="breadcrumb">
            <li class="active">
                <i class="fa fa-dashboard"></i> <a href="${request.application_url}"> Strona główna </a>
            </li>
            <li class="active">
                <i class="fa fa-bar-chart-o"></i> Nowe zamówienie
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->

% if not order_done:

    <form role="form" method="post" action="${request.application_url}/order_new">
        <div class="col-lg-5">

            <div class="clone">
                <div class="form-group">
                    <label>Produkt 1</label>
                    <select name="product_1" class="form-control">
                        % for product in product_list:
                            <option value="${product[1]}">${product[0]}</option>
                        % endfor
                    </select>
                </div>
                <div class="form-group">
                    <label>Ilość</label>
                    <input name="quantity_1" class="form-control">
                </div>
                <hr>
                <button type="button" class="btn btn-primary clone_button">Kolejny produkt</button>
            </div><br>

            <div class="form-group">
                <label>Sposób płatności</label>
                <select name="payment" class="form-control">
                    % for payment in payment_list:
                        <option value="${payment[1]}">${payment[0]}</option>
                    % endfor
                </select>
            </div>

            <div class="form-group">
                <label>Sposób dostawy</label>
                <select name="delivery" class="form-control">
                    % for delivery in delivery_list:
                        <option value="${delivery[1]}">${delivery[0]}</option>
                    % endfor
                </select>
            </div>

            <div class="form-group">
                <label>Adres</label>
                <select name="address" class="form-control">
                    % for address in address_list:
                        <option value="${address[1]}">${address[0]}</option>
                    % endfor
                </select>

                <div class="checkbox">
                    <label>
                        <input name="change_address" type="checkbox"> Inny adres
                    </label>
                </div>

                <div class="form-group other_address">
                    <label> Ulica </label>
                    <input class="form-control required" name="street">
                </div>
                <div class="form-group other_address">
                    <label> Numer domu </label>
                    <input class="form-control required" name="house_number">
                </div>
                <div class="form-group other_address">
                    <label> Numer mieszkania </label>
                    <input class="form-control" name="flat_number">
                </div>
                <div class="form-group other_address">
                    <label> Kod pocztowy </label>
                    <input class="form-control required" name="postal_code">
                </div>
                <div class="form-group other_address">
                    <label> Miasto </label>
                    <input class="form-control required" name="city">
                </div>
                <div class="form-group other_address">
                    <label> Kraj </label>
                    <input class="form-control required" name="country">
                </div>

            </div>

            <button type="submit" class="btn btn-success"> Złóż zamówienie </button>
        </div>
    </form>

% else:
    % if order_success:
        <div class="alert alert-success">
            <strong>Zamówienie zostało złożone.</strong></br>
            Numer zamówienia: ${order_id}
        </div>
    % else:
        <div class="alert alert-danger">
            Zamówienie nie zostało złożone. Proszę spróbować jeszcze raz.
        </div>
    % endif
% endif

<script type="text/javascript">

$(document).ready(function(){
    $('button.clone_button').on('click', function(){
        $this = $(this);

        var clonedProduct = $('.clone select[name*="product"]').parent().last().clone(false);
        var clonedQuantity = $('.clone input[name*="quantity"]').parent().last().clone(false);

        actualClone = parseInt(clonedProduct.find('label').html().split(' ')[1]);
        nextClone = actualClone + 1;

        clonedProduct.find('label').html(
            clonedProduct.find('label').html().replace(actualClone, nextClone)
        );
        clonedProduct.find('select').prop(
            'name',
            clonedProduct.find('select').prop('name').replace(actualClone, nextClone)
        );
        clonedProduct.find('select').prop('value', null);

        clonedQuantity.find('input').prop(
            'name',
            clonedQuantity.find('input').prop('name').replace(actualClone, nextClone)
        );
        clonedQuantity.find('input').prop('value', null);

        clonedProduct
            .removeClass('clone_'+actualClone)
            .addClass('clone_'+nextClone);
        clonedQuantity
            .removeClass('clone_'+actualClone)
            .addClass('clone_'+nextClone);
        clonedProduct.insertBefore($this);
        clonedQuantity.insertBefore($this);

        $(document.createElement('button'))
            .html('Usuń produkt '+nextClone)
            .prop('type', 'button')
            .addClass('btn btn-danger clone_'+nextClone+' remove_clone_'+nextClone)
            .on('click', removeClone)
            .insertBefore($this);
        $(document.createElement('br'))
            .addClass('clone_'+nextClone)
            .insertBefore($this);
        $(document.createElement('hr'))
            .addClass('clone_'+nextClone)
            .insertBefore($this);
    });

    function removeClone() {
        cloneNumber = this.classList[3].split('_')[2];
        $('.clone_'+cloneNumber).remove();
    }

    function checkOtherAddress() {
        $otherAddressFields = $('.other_address');
        $requiredFields = $otherAddressFields.find('input[class*="required"]');
        if ($(this).prop('checked')){
            $otherAddressFields.show();
            $otherAddressFields.find('input').prop('disabled', false);
            $requiredFields.prop('required', true);
            $('select[name="address"]').prop('disabled', true);
        }
        else {
            $otherAddressFields.hide();
            $otherAddressFields.find('input').prop('disabled', true);
            $requiredFields.prop('required', false);
            $('select[name="address"]').prop('disabled', false);
        }
    }

    function disableAddressFields() {
        if (this.value === '1'){  // Odbiór osobisty
            $('select[name="address"]').prop('disabled', true);
            $('input[name="change_address"]').prop('disabled', true);
            $('.other_address input').prop('disabled', true);
        }
        else {
            $('select[name="address"]').prop('disabled', false);
            $('input[name="change_address"]').prop('disabled', false);
            if ($('input[name="change_address"]').prop('checked')){
                $('.other_address input').prop('disabled', false);
            }
        }
    }

    $('input[name="change_address"]').on('change', checkOtherAddress);
    $('select[name="delivery"]').on('change', disableAddressFields);

    $(document).ready(function(){
        $('input[name="change_address"]').trigger('change');
        $('select[name="delivery"]').trigger('change');
    });
})

</script>

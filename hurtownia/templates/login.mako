<!DOCTYPE html>
<%inherit file="base.mako"/>

<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"> Logowanie </h1>
    </div>
</div>
<!-- /.row -->

<form role="form" method="post" action="${request.application_url}/login">
    <div class="col-lg-3">
        <div class="form-group">
            <label> Email: </label>
            <input class="form-control" placeholder="Wprowadź email" type="text" name="email" maxlength="255" required>
        </div>
        <div class="form-group">
            <label> Hasło: </label>
            <input class="form-control" placeholder="Wprowadź hasło" type="password" name="password" maxlength="20" required>
        </div>
        <button type="submit" class="btn btn-success"> Zaloguj </button>
        <button type="reset" class="btn btn-danger"> Wyczyść </button>
    </div>
</form>

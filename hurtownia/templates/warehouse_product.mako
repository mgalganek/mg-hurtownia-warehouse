<!DOCTYPE html>

<%inherit file="base.mako"/>

<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"> Przegląd Produktu </h1>
        <ol class="breadcrumb">
            <li class="active">
                <i class="fa fa-dashboard"></i> <a href="${request.application_url}"> Strona główna </a>
            </li>
            <li class="active">
                <i class="fa fa-bar-chart-o"></i> <a href="${request.application_url}/warehouse"> Przegląd Magazynu </a>
            </li>
            <li class="active">
                <i class="fa fa-bar-chart-o"></i> Przegląd Produktu
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->

<div class="col-lg-8">
    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>
                  <th>Status</th>
                  <th>Nazwa</th>
                  <th>Cena</th>
                  <th>Ilość</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>${product.Status_str}</td>
                    <td>${product.Name}</td>
                    <td>${product.Price}</td>
                    <td>${product.Quantity}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="col-lg-8">
    <div class="table-responsive">
        <table class="table table-hover">
            % if unity:
                <thead>
                    <tr>
                        <th>Jednostka</th>
                        <th>Nazwa</th>
                        <th>Krótka nazwa</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>-</th>
                        <td>${unity.Name}</td>
                        <td>${unity.ShortName}</td>
                    </tr>
                </tbody>
            % else:
                <thead>
                    <tr>
                        <th>Jednostka</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>Brak jednostki produktu</th>
                    </tr>
                </tbody>
            % endif
        </table>
    </div>
</div>


<div class="col-lg-8">
    <div class="table-responsive">
        <table class="table table-hover">
            % if vat_rate:
                <thead>
                    <tr>
                        <th>Stawka Vat</th>
                        <th>Nazwa</th>
                        <th>Wartość</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>-</th>
                        <td>${vat_rate.Name}</td>
                        <td>${vat_rate.Value}</td>
                    </tr>
                </tbody>
            % else:
                <thead>
                    <tr>
                        <th>Stawka Vat</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>Brak vatu produktu</th>
                    </tr>
                </tbody>
            % endif
        </table>
    </div>
</div>


<div class="col-lg-8">
    <div class="table-responsive">
        <table class="table table-hover">
            % if products_properties_list:
                <thead>
                    <tr>
                        <th>Własności</th>
                        <th>Nazwa</th>
                        <th>Wartość</th>
                    </tr>
                </thead>
                <tbody>
                    % for products_property in products_properties_list:
                        <tr>
                            <th>-</th>
                            <td>${products_property[0]}</td>
                            <td>${products_property[0]}</td>
                        </tr>
                    % endfor
                </tbody>
            % else:
                <thead>
                    <tr>
                        <th>Własności</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>Brak własności produktu</th>
                    </tr>
                </tbody>
            % endif
        </table>
    </div>
</div>

# -*- coding: utf-8 -*-

from pyramid.config import Configurator
from pyramid.authentication import AuthTktAuthenticationPolicy
from pyramid.authorization import ACLAuthorizationPolicy
from hurtownia.src.security import groupfinder
from hurtownia.src.models import root_factory


def main(global_config, **settings):
    u"""This function returns a Pyramid WSGI application."""

    authn_policy = AuthTktAuthenticationPolicy(
        'sosecret',
        callback=groupfinder,
        hashalg='sha512'
    )
    authz_policy = ACLAuthorizationPolicy()
    config = Configurator(root_factory=root_factory, settings=settings)
    config.include('pyramid_mako')
    config.include('pyramid_chameleon')
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_route('home', '/')
    config.add_route('check_login', 'check_login')
    config.add_route('login', 'login')
    config.add_route('logout', 'logout')
    config.add_route('order_new', 'order_new')
    config.add_route('order_list', 'order_list')
    config.add_route('order_cancel', 'order_cancel')
    config.add_route('order_edit', 'order_edit')
    config.add_route('user_details', 'user_details')
    config.add_route('warehouse', 'warehouse')
    config.add_route('warehouse_product', 'warehouse/{product}')
    config.add_route('order_history', 'order_history')
    config.add_route('order_details', 'order_details/{order}')
    config.add_route('api', 'api')
    config.add_route('password_change', 'password_change')
    config.set_authentication_policy(authn_policy)
    config.set_authorization_policy(authz_policy)
    config.scan()
    return config.make_wsgi_app()

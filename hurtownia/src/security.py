# -*- coding: utf-8 -*-

from hurtownia.src.models import SESSION, Contractor


def groupfinder(userid, request):
    u"""Function that finds group for user."""
    session = SESSION()
    if userid not in [u.Email for u in session.query(Contractor).all()]:
        session.close()
        return None
    else:
        session.close()
        return ['logged']

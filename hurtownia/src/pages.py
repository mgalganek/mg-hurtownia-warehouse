from webhelpers import paginate


def paginagte(
    query, request, items_per_page=10, *args, **kwargs
):
    page_url = paginate.PageURL_WebOb(request)
    page = int(request.params.get("page", 1))
    return paginate.Page(
        query, page=page, url=page_url, items_per_page=items_per_page
    )

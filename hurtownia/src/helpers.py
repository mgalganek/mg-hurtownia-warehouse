# -*- coding: utf-8 -*-

import urlparse
from sqlalchemy import desc

from hurtownia.src.models import (
    SESSION,
    Contractor,
    Product,
    ProductProperty,
    Unity,
    VatRate,
    Order,
    OrderPosition
)


def get_user_name(email):
    session = SESSION()
    for contractor in session.query(Contractor).all():
        if contractor.Email == email:
            session.close()
            return contractor.Name

    session.close()
    return


def get_user_details(email):
    session = SESSION()
    results = session.query(Contractor).filter(Contractor.Email == email).one()
    session.close()
    return results


def get_all_products(order_id=None):
    session = SESSION()

    if order_id:
        orders_postion = session.query(OrderPosition).filter(
            OrderPosition.OrderId == order_id
        ).all()
        results = [
            (postion.Product, postion.Quantity) for postion in orders_postion
        ]
    else:
        results = session.query(Product).order_by(desc(Product.Id)).all()

    session.close()
    return results


def get_product_by(product_id):
    session = SESSION()
    results = session.query(Product).filter(Product.Id == product_id).one()
    session.close()
    return results


def get_all_orders_by_contractor(contractor_id):
    session = SESSION()
    results = session.query(Order).filter(
        Order.ContractorId == contractor_id).order_by(desc(Order.Id)).all()

    orders_with_address_delivery_payment = [
        (order, order.Address, order.Delivery, order.Payment)
        for order in results
    ]

    session.close()
    return orders_with_address_delivery_payment


def get_order_by(order_id):
    session = SESSION()
    order = session.query(Order).filter(Order.Id == order_id).one()
    results = (order, order.Address, order.Delivery, order.Payment)
    session.close()
    return results


def get_product_properties_by_product(product_id):
    session = SESSION()
    results = session.query(ProductProperty).filter(
        ProductProperty.ProductId == product_id).all()
    product_properties_with_name_and_type = [
        [product_property.Property.Name, product_property.Property.Type]
        for product_property in results
    ]
    session.close()
    return product_properties_with_name_and_type


def get_unity_by(unity_id):
    session = SESSION()
    results = session.query(Unity).filter(Unity.Id == unity_id).first()
    session.close()
    return results


def get_vat_rate_by(vat_rate_id):
    session = SESSION()
    results = session.query(VatRate).filter(VatRate.Id == vat_rate_id).first()
    session.close()
    return results


def get_url_param(url, param):
    url_params = urlparse.parse_qs(urlparse.urlparse(url).query)
    return url_params.get(param)[0]


def get_address_repr(addr):
    return ' '.join([
        addr.Street,
        '/'.join([addr.HouseNumber, addr.FlatNumber]),
        addr.PostalCode,
        addr.City,
        addr.Country,
    ])


def get_product_repr(product):
    return ' '.join([
        product.Name,
        '{:.2f} PLN'.format(product.Price).replace('.', ',')
    ])


def get_order_amount(order=None, order_id=None):
    if not order_id:
        order_id = order.Id
    session = SESSION()
    order_positions = session.query(OrderPosition).filter(
        OrderPosition.OrderId == order_id
    ).all()
    amount = 0
    for position in order_positions:
        product = session.query(Product).filter(
            Product.Id == position.ProductId
        ).one()
        # TODO: Uwzględnienie zniżki
        amount += product.Price * position.Quantity
    session.close()
    return '{:.2f} PLN'.format(amount).replace('.', ',')


def get_order_count_by_status(status, contractor_id):
    session = SESSION()
    orders_count = session.query(Order).filter(
        Order.Status == status
    ).filter(
        Order.ContractorId == contractor_id
    ).count()
    session.close()
    return orders_count

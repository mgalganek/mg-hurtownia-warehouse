# -*- coding: utf-8 -*-

from datetime import datetime

import sqlalchemy as sa
from pyramid.security import Allow, Deny, Everyone
from sqlalchemy import (
    Column,
    create_engine,
    Date,
    DateTime,
    ForeignKey,
    Integer,
    Numeric,
    String
)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker

from hurtownia.config import config

BASE = declarative_base()
DB_URL = 'mysql://{login}:{password}@{host}/{name}?charset=utf8'.format(
    login=config.DB_LOGIN,
    password=config.DB_PASSWORD,
    host=config.DB_HOST,
    name=config.DB_NAME
)
ENGINE = create_engine(DB_URL, echo=False, pool_recycle=3600)
SESSION = sessionmaker(bind=ENGINE)
BASE.metadata.create_all(ENGINE)


class Revision(BASE):
    __tablename__ = 'Revision'

    Id = Column(Integer, primary_key=True, unique=True, nullable=False)
    DateTime = Column(DateTime, nullable=False)
    AppVersion = Column(Integer, nullable=False)
    UserId = Column(Integer, ForeignKey('Contractor.Id'))

    def __init__(self, AppVersion, UserId):
        self.DateTime = datetime.now()
        self.AppVersion = AppVersion
        self.UserId = UserId


class OrderRevision(BASE):
    __tablename__ = 'Order_Revision'
    IdWeb = Column(Integer, nullable=False, primary_key=True, autoincrement=True)
    Id = Column(Integer, nullable=False, primary_key=True, autoincrement=False)
    Status = Column(Integer, nullable=False)
    DateIssue = Column(Date, nullable=False)
    AddressId = Column(Integer, ForeignKey('Address.Id'), nullable=True)
    DeliveryId = Column(Integer, ForeignKey('Delivery.Id'), nullable=False)
    PaymentId = Column(Integer, ForeignKey('Payment.Id'), nullable=False)
    ContractorId = Column(Integer, ForeignKey('Contractor.Id'), nullable=False)
    RevisionId = Column(Integer, nullable=False)
    RevisionType = Column(Integer, nullable=False, default='1')


class SessionMixin(object):

    def save(self, commit=True, change_type=None):
        db = SESSION()
        db.add(self)
        db.commit()
        db.refresh(self)
        db.close()

        if change_type:
            db = SESSION()
            if change_type == 'insert':
                rev_value = 0
            elif change_type == 'update':
                rev_value = 1
            elif change_type == 'delete':
                rev_value = 2

            # OrderRevision
            if self.__tablename__ == 'Order':
                rev = Revision(
                    AppVersion='0.0.1',
                    UserId=self.ContractorId
                )
                db.add(rev)
                db.commit()
                db.refresh(rev)

                order_revision = OrderRevision(
                    Id=self.Id,
                    RevisionId=rev.Id,
                    Status=self.Status,
                    DateIssue=self.DateIssue,
                    AddressId=self.AddressId,
                    DeliveryId=self.DeliveryId,
                    PaymentId=self.PaymentId,
                    ContractorId=self.ContractorId,
                    RevisionType=rev_value,
                )

                db.add(order_revision)
                db.commit()
                db.close()

    def destroy(self):
        db = SESSION()
        db.delete(self)
        db.commit()
        db.close()


class root_factory(object):
    u"""Functions contains the ACL definition."""
    __acl__ = [
        (Allow, 'logged', 'use'),
        (Deny, Everyone, 'use'),
    ]

    def __init__(self, request):
        pass


class Contractor(BASE, SessionMixin):
    __tablename__ = 'Contractor'

    Id = Column(Integer, primary_key=True, unique=True, nullable=False)
    Name = Column(String(100), nullable=False)
    Nip = Column(String(10), unique=True, nullable=False)
    Regon = Column(String(10), unique=True, nullable=False)
    Pesel = Column(String(11), unique=True, nullable=False)
    Email = Column(String(255), unique=True, nullable=False)
    Password = Column(String(255), nullable=False)


class Address(BASE, SessionMixin):
    __tablename__ = 'Address'

    Id = Column(Integer, primary_key=True, unique=True, nullable=False)
    Street = Column(String(255), nullable=False)
    HouseNumber = Column(String(10), nullable=False)
    FlatNumber = Column(String(10), nullable=True)
    PostalCode = Column(String(10), nullable=False)
    City = Column(String(255), nullable=False)
    Country = Column(String(100), nullable=False)
    ContractorId = Column(Integer, ForeignKey('Contractor.Id'), nullable=False)

    def __init__(self, street, house_number, flat_number, postal_code, city,
                 country, contractor_id):
        self.Street = street
        self.HouseNumber = house_number
        self.FlatNumber = flat_number
        self.PostalCode = postal_code
        self.City = city
        self.Country = country
        self.ContractorId = contractor_id


class Delivery(BASE):
    __tablename__ = 'Delivery'

    Id = Column(Integer, primary_key=True, unique=True, nullable=False)
    Method = Column(String(35), nullable=False)
    Cost = Column(Numeric, nullable=False)


class Payment(BASE):
    __tablename__ = 'Payment'

    Id = Column(Integer, primary_key=True, unique=True, nullable=False)
    Method = Column(String(35), nullable=False)


class Order(BASE, SessionMixin):
    __tablename__ = 'Order'

    Id = Column(Integer, primary_key=True, unique=True, nullable=False)
    Status = Column(Integer, nullable=False)
    DateIssue = Column(Date, nullable=False)
    AddressId = Column(Integer, ForeignKey('Address.Id'), nullable=True)
    DeliveryId = Column(Integer, ForeignKey('Delivery.Id'), nullable=False)
    PaymentId = Column(Integer, ForeignKey('Payment.Id'), nullable=False)
    ContractorId = Column(Integer, ForeignKey('Contractor.Id'), nullable=False)

    @property
    def Status_str(self):
        if self.Status == 1:
            return u'Nowe'
        elif self.Status == 2:
            return u'Anulowane'
        elif self.Status == 3:
            return u'Zrealizowane'

    Address = relationship(
        'Address', primaryjoin=AddressId == Address.Id,
        foreign_keys=[Address.Id], uselist=False
    )
    Delivery = relationship(
        'Delivery', primaryjoin=DeliveryId == Delivery.Id,
        foreign_keys=[Delivery.Id], uselist=False
    )
    Payment = relationship(
        'Payment', primaryjoin=PaymentId == Payment.Id,
        foreign_keys=[Payment.Id], uselist=False
    )

    def __init__(self, status, date_issue, address_id, delivery_id,
                 payment_id, contractor_id):
        self.Status = status
        self.DateIssue = date_issue
        self.AddressId = address_id
        self.DeliveryId = delivery_id
        self.PaymentId = payment_id
        self.ContractorId = contractor_id


class Product(BASE):
    __tablename__ = 'Product'

    Id = Column(Integer, primary_key=True, unique=True, nullable=False)
    Status = Column(Integer, nullable=False)
    Name = Column(String(255), nullable=False)
    VatRateId = Column(Integer, nullable=False)
    UnityId = Column(Integer, nullable=False)
    Price = Column(Numeric, nullable=False)
    Quantity = Column(Integer, nullable=False)

    @property
    def Status_str(self):
        if self.Status == 0:
            return u'Niedostępny'
        elif self.Status == 1:
            return u'Dostępny'


class Property(BASE):
    __tablename__ = 'Property'

    Id = Column(Integer, primary_key=True, unique=True, nullable=False)
    Name = Column(String(255), nullable=False)
    Type = Column(String(255), nullable=False)


class ProductProperty(BASE):
    __tablename__ = 'ProductProperty'

    Id = Column(Integer, primary_key=True, unique=True, nullable=False)
    ProductId = Column(Integer, ForeignKey('Product.Id'), nullable=False)
    PropertyId = Column(Integer, ForeignKey('Property.Id'), nullable=False)
    Value = Column(String(255), nullable=False)

    Product = relationship(
        'Product', primaryjoin=ProductId == Product.Id,
        foreign_keys=[Product.Id], uselist=False
    )
    Property = relationship(
        'Property', primaryjoin=PropertyId == Property.Id,
        foreign_keys=[Property.Id], uselist=False
    )


class Unity(BASE):
    __tablename__ = 'Unity'

    Id = Column(Integer, primary_key=True, unique=True, nullable=False)
    Name = Column(String(20), nullable=False)
    ShortName = Column(String(5), nullable=False)


class VatRate(BASE):
    __tablename__ = 'VatRate'

    Id = Column(Integer, primary_key=True, unique=True, nullable=False)
    Name = Column(String(3), nullable=False)
    Value = Column(Numeric, nullable=False)


class OrderPosition(BASE, SessionMixin):
    __tablename__ = 'OrderPosition'

    Id = Column(Integer, primary_key=True, unique=True, nullable=False)
    OrderId = Column(Integer, ForeignKey('Order.Id'), nullable=False)
    ProductId = Column(Integer, ForeignKey('Product.Id'), nullable=False)
    # DiscountId = Column(Integer, ForeignKey('DiscountId.Id'), nullable=True)
    DiscountId = Column(Integer, nullable=True)
    Quantity = Column(Integer, nullable=False)
    DocumentType = Column(Integer, nullable=False)
    DocumentPositionId = Column(Integer, nullable=False)

    def __init__(self, order_id, product_id, quantity, discount_id=None,
                 document_type=None, document_position_id=None):
        self.OrderId = order_id
        self.ProductId = product_id
        self.Quantity = quantity
        self.DiscountId = discount_id
        self.DocumentType = document_type
        self.DocumentPositionId = document_position_id

    Product = relationship(
        'Product', primaryjoin=ProductId == Product.Id,
        foreign_keys=[Product.Id], uselist=False
    )

sa.orm.configure_mappers()

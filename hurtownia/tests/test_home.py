from hurtownia.tests.testing import ViewTests
from hurtownia.views.home import home_view


class HomeTests(ViewTests):

    def test_home(self):
        info = home_view(self.request)
        self.assertEqual(info['user'], ' ')

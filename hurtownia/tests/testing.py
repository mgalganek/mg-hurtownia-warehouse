from sqlalchemy import create_engine
from hurtownia.src.models import SESSION, BASE

import unittest

from pyramid import testing

from sqlalchemy.orm import (
    scoped_session,
    sessionmaker
)
from sqlalchemy.ext.declarative import declarative_base
from zope.sqlalchemy import ZopeTransactionExtension

db_session = scoped_session(sessionmaker(extension=ZopeTransactionExtension()))
Base = declarative_base()


class ViewTests(unittest.TestCase):

    request = testing.DummyRequest()
    maxDiff = None

    def setUp(self):
        self.session = self.init_testing_db()
        self.config = testing.setUp()
        self.config.include('pyramid_mako')
        self.config.scan()
        self.request.params = {}
        self.request.POST = {}

    def tearDown(self):
        self.session.remove()
        testing.tearDown()

    def init_testing_db(self):
        """Create testing database"""
        engine = create_engine('sqlite:///:memory:')
        BASE.metadata.create_all(engine)
        db_session.configure(bind=engine)
        return db_session

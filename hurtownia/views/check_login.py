# -*- coding: utf-8 -*-

from pyramid.view import view_config, forbidden_view_config
from pyramid.security import authenticated_userid
from pyramid.httpexceptions import HTTPFound


@view_config(route_name='check_login', renderer='login.mako')
@forbidden_view_config(renderer='login.mako')
def check_login(request):
    u"""View for initiate login."""

    if authenticated_userid(request) is not None:
        return HTTPFound(location='/')

    return {}

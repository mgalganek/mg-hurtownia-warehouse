# -*- coding: utf-8 -*-

from pyramid.security import authenticated_userid
from pyramid.view import view_config

from hurtownia.src.helpers import get_all_products, get_user_name
from hurtownia.src.pages import paginagte


@view_config(
    route_name='warehouse', renderer='warehouse.mako', permission='use')
def warehouse(request):
    u"""Review warehouse"""
    products = get_all_products()
    logged_user = authenticated_userid(request)
    user_name = get_user_name(logged_user)
    return {
        'products': paginagte(query=products, request=request),
        'user': user_name
    }

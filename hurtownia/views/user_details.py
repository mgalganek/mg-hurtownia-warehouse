# -*- coding: utf-8 -*-

from pyramid.view import view_config
from pyramid.security import authenticated_userid

from hurtownia.src.helpers import (
    get_user_details,
    get_user_name
)


@view_config(route_name='user_details', renderer='user_details.mako')
def user_details(request):
    u"""Worker details"""

    email = authenticated_userid(request)
    user_name = get_user_name(email)
    user_object = get_user_details(email)
    return {'user_object': user_object, 'user': user_name}

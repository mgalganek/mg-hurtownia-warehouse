# -*- coding: utf-8 -*-

from pyramid.view import view_config
from hurtownia.src.helpers import get_all_products

import simplejson
 

@view_config(route_name='api', renderer='json')
def home_view(request):
    u"""Home page."""

    result = get_all_products()

    ret_list = []
    for item in result:
        ret_list.append({
            'name': item.Name,
            'price': str(item.Price)
        })

    return {
        'products': ret_list
    }

# -*- coding: utf-8 -*-

from pyramid.view import view_config
from pyramid.security import forget, authenticated_userid
from pyramid.httpexceptions import HTTPFound

from hurtownia.src.helpers import get_user_name


@view_config(route_name='logout')
def logout(request):
    u"""View for logout."""

    logged_user = authenticated_userid(request)
    user_name = get_user_name(logged_user)

    print user_name, "is unlogged"
    header = forget(request)

    return HTTPFound(location='check_login', headers=header)

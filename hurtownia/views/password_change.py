# -*- coding: utf-8 -*-

import md5
from base64 import b64encode

from pyramid.view import view_config
from pyramid.security import authenticated_userid

from hurtownia.src.helpers import (
    get_user_name,
    get_user_details
)


@view_config(
    route_name='password_change',
    renderer='password_change.mako',
    permission='use'
)
def home_view(request):
    u"""Password change page."""

    logged_user = authenticated_userid(request)
    user_name = get_user_name(logged_user)

    result = {
        'user': user_name,
        'success': False,
        'message': '',
    }

    if not request.POST:
        return result

    old_pswd = request.POST.get('old_pswd')
    old_pswd_md5 = b64encode(md5.new(old_pswd).digest())
    new_pswd = request.POST.get('new_pswd')
    repeat_new_pswd = request.POST.get('repeat_new_pswd')

    user = get_user_details(logged_user)

    if user.Password != old_pswd_md5:
        message = u'Nieprawidłowe hasło.'
    elif new_pswd != repeat_new_pswd:
        message = u'Podane hasła się nie zgadzają.'
    elif old_pswd == new_pswd:
        message = u'Stare hasło i nowe hasło są takie same.'
    else:
        user.Password = b64encode(md5.new(new_pswd).digest())
        user.save(commit=False)

        message = u'Zmiana hasła powiodła się.'
        result.update({
            'success': True,
        })

    result.update({
        'message': message
    })
    return result

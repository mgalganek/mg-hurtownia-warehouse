# -*- coding: utf-8 -*-

from pyramid.httpexceptions import HTTPFound
from pyramid.security import authenticated_userid
from pyramid.view import view_config

from hurtownia.src.helpers import get_url_param, get_user_name
from hurtownia.src.models import Order, SESSION


@view_config(
    route_name='order_cancel', renderer='order_cancel.mako', permission='use')
def order_cancel(request):
    u"""Order cancel page."""
    logged_user = authenticated_userid(request)
    user_name = get_user_name(logged_user)
    order_id = get_url_param(request.url, 'order_id')
    result = {
        'order_id': order_id,
        'user': user_name,
    }

    if not request.POST:
        result['confirmed'] = False
        return result

    elif request.POST.get('return'):
        return HTTPFound(location='order_list')

    elif request.POST.get('cancel_order'):
        result['confirmed'] = True
        try:
            session = SESSION()
            order = session.query(Order).filter(Order.Id == order_id).one()
            session.close()
            order.Status = 2  # Anulowane
            order.save(commit=False, change_type='delete')
            result['cancel_success'] = True
        except:
            result['cancel_success'] = False

        return result

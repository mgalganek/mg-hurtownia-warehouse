# -*- coding: utf-8 -*-

from datetime import date

from pyramid.security import authenticated_userid
from pyramid.view import view_config

from hurtownia.src.helpers import (
    get_address_repr,
    get_product_repr,
    get_user_details,
    get_user_name
)
from hurtownia.src.models import (
    Address,
    Delivery,
    Order,
    OrderPosition,
    Payment,
    Product,
    SESSION
)


def get_order_position_data(data):
    result = []
    for item in data.items():
        if not item[0].startswith('product'):
            continue
        number = item[0].split('_')[1]
        quantity_item = [
            i for i in data.items() if i[0] == 'quantity_' + number
        ][0]
        if not quantity_item[1]:
            continue
        for pos in result:
            if pos['product'] == item[1]:
                pos['quantity'] = int(pos['quantity']) + int(quantity_item[1])
                break
        else:
            result.append({
                'product': item[1],
                'quantity': quantity_item[1]
            })
    return result


@view_config(
    route_name='order_new', renderer='order_new.mako', permission='use')
def order_new(request):
    u"""New order page."""
    # TODO: Dynamiczne przeliczanie ceny na zamówieniu.
    logged_user = authenticated_userid(request)
    user_name = get_user_name(logged_user)
    contractor_id = get_user_details(logged_user).Id
    result = {
        'user': user_name,
        'order_done': False,
        'order_success': False,
    }
    if not request.POST:
        session = SESSION()
        address_list = [
            (get_address_repr(p), p.Id)
            for p in session.query(Address).filter(
                Address.ContractorId == contractor_id
            ).all()
        ]
        delivery_list = [
            (p.Method, p.Id)
            for p in session.query(Delivery).all()
        ]
        payment_list = [
            (p.Method, p.Id)
            for p in session.query(Payment).all()
        ]
        product_list = [
            (get_product_repr(p), p.Id)
            for p in session.query(Product).all() if p.Status == 1
        ]

        result.update({
            'address_list': address_list,
            'delivery_list': delivery_list,
            'payment_list': payment_list,
            'product_list': product_list,
        })
        session.close()
        return result
    else:
        order_position_data = get_order_position_data(request.POST)
        if not order_position_data:
            result.update({
                'order_done': True,
            })
            return result

        if request.POST.get('delivery') == '1':  # Odbiór osobisty
            address_id = None
        elif request.POST.get('change_address'):
            address = Address(
                street=request.POST.get('street'),
                house_number=request.POST.get('house_number'),
                flat_number=request.POST.get('flat_number'),
                postal_code=request.POST.get('postal_code'),
                city=request.POST.get('city'),
                country=request.POST.get('country'),
                contractor_id=contractor_id,
            )
            address.save(commit=False)
            address_id = address.Id
        else:
            address_id = request.POST.get('address')

        order = Order(
            status=1,  # Nowe
            date_issue=date.today(),
            address_id=address_id,
            delivery_id=request.POST.get('delivery'),
            payment_id=request.POST.get('payment'),
            contractor_id=contractor_id,
        )
        order.save(commit=False, change_type='insert')

        for position in order_position_data:
            # TODO: pobranie odpowiedniej zniżki
            OrderPosition(
                order_id=order.Id,
                product_id=position['product'],
                quantity=position['quantity'],
            ).save()

        logged_user = authenticated_userid(request)
        user_name = get_user_name(logged_user)
        result.update({
            'order_id': order.Id,
            'order_done': True,
            'order_success': True,
        })
        return result

# -*- coding: utf-8 -*-

from pyramid.security import authenticated_userid
from pyramid.view import view_config

from hurtownia.src.helpers import (
    get_order_amount,
    get_user_details,
    get_user_name
)
from hurtownia.src.models import (
    Address,
    Order,
    OrderPosition,
    Product,
    SESSION
)
from hurtownia.src.pages import paginagte


@view_config(
    route_name='order_list', renderer='order_list.mako', permission='use')
def order_list(request):
    u"""Order list page."""
    # TODO: zamówienia bez adresu
    logged_user = authenticated_userid(request)
    user_name = get_user_name(logged_user)
    contractor_id = get_user_details(logged_user).Id

    session = SESSION()
    order_list = [
        {
            'number': p.Id,
            'date': p.DateIssue,
            'amount': get_order_amount(order=p),
            'edit_url': 'order_edit?order_id={}'.format(p.Id),
            'cancel_url': 'order_cancel?order_id={}'.format(p.Id),
        }
        for p in session.query(Order).filter(
            Order.ContractorId == contractor_id
        )
        .filter(Order.Status == 1)
        .all()
    ]
    session.close()
    return {
        'order_list': paginagte(query=order_list, request=request),
        'user': user_name,
    }

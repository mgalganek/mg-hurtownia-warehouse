# -*- coding: utf-8 -*-

from pyramid.view import view_config
from pyramid.security import authenticated_userid

from hurtownia.src.helpers import (
    get_user_name,
    get_order_count_by_status,
    get_user_details
)


@view_config(route_name='home', renderer='home.mako', permission='use')
def home_view(request):
    u"""Home page."""

    user_email = authenticated_userid(request)
    user = get_user_details(user_email)

    return {
        'user': get_user_name(user_email),
        'count_new': get_order_count_by_status(1, user.Id),  # Nowe
        'count_canceled': get_order_count_by_status(2, user.Id),  # Anulowane
        'count_done': get_order_count_by_status(3, user.Id),  # Zrealizowane
    }

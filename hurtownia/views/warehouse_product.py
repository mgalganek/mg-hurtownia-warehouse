# -*- coding: utf-8 -*-

from pyramid.view import view_config
from pyramid.security import authenticated_userid

from hurtownia.src.helpers import (
    get_product_by,
    get_product_properties_by_product,
    get_unity_by,
    get_vat_rate_by,
    get_user_name
)


@view_config(
    route_name='warehouse_product',
    renderer='warehouse_product.mako',
    permission='use'
)
def warehouse_product(request):
    u"""Single product from warehouse"""
    logged_user = authenticated_userid(request)
    user_name = get_user_name(logged_user)

    product_id = request.matchdict.get('product')
    product = get_product_by(product_id)
    list_of_products_properties = get_product_properties_by_product(product_id)
    unity = get_unity_by(product.UnityId)
    vat_rate = get_vat_rate_by(product.VatRateId)

    return {
        'product': product,
        'products_properties_list': list_of_products_properties,
        'unity': unity,
        'vat_rate': vat_rate,
        'user': user_name
    }

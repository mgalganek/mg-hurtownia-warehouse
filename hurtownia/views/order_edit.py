# -*- coding: utf-8 -*-

from datetime import date

from pyramid.httpexceptions import HTTPFound
from pyramid.security import authenticated_userid
from pyramid.view import view_config

from hurtownia.src.helpers import (
    get_address_repr,
    get_product_repr,
    get_url_param,
    get_user_details,
    get_user_name
)
from hurtownia.src.models import (
    Address,
    Delivery,
    Order,
    OrderPosition,
    Payment,
    Product,
    SESSION
)
from hurtownia.views.order_new import get_order_position_data


@view_config(
    route_name='order_edit', renderer='order_edit.mako', permission='use')
def order_edit(request):
    u"""Order edit page."""
    logged_user = authenticated_userid(request)
    user_name = get_user_name(logged_user)
    contractor_id = get_user_details(logged_user).Id
    order_id = get_url_param(request.url, 'order_id')
    result = {
        'order_id': order_id,
        'user': user_name,
        'order_done': False,
        'order_success': False,
    }

    if not request.POST:
        session = SESSION()
        order = session.query(Order).filter(Order.Id == order_id).one()
        order_positions = session.query(OrderPosition).filter(
            OrderPosition.OrderId == order_id
        ).all()
        address = session.query(Address).filter(
            Address.Id == order.AddressId
        ).one() if order.AddressId else None

        address_list = [
            (get_address_repr(p), p.Id)
            for p in session.query(Address).filter(
                Address.ContractorId == contractor_id
            ).all()
        ]
        delivery_list = [
            (p.Method, p.Id)
            for p in session.query(Delivery).all()
        ]
        payment_list = [
            (p.Method, p.Id)
            for p in session.query(Payment).all()
        ]
        product_list = [
            (get_product_repr(p), p.Id)
            for p in session.query(Product).all() if p.Status == 1
        ]
        session.close()

        model2dict = lambda r: {
            c.name: getattr(r, c.name) for c in r.__table__.columns
        }

        result.update({
            'address_list': address_list,
            'delivery_list': delivery_list,
            'payment_list': payment_list,
            'product_list': product_list,
            'order': model2dict(order),
            'order_positions': [model2dict(p) for p in order_positions],
            'address': model2dict(address) if address else None,
        })
        return result

    elif request.POST.get('return'):
        return HTTPFound(location='order_list')

    elif request.POST.get('save'):
        session = SESSION()
        actual_positions = session.query(OrderPosition).filter(
            OrderPosition.OrderId == order_id
        ).all()
        session.close()
        updated_positions = get_order_position_data(request.POST)

        session = SESSION()
        order = session.query(Order).filter(Order.Id == order_id).one()
        session.close()
        delivery_changed = False

        payment = request.POST.get('payment')
        if str(order.PaymentId) != payment:
            order.PaymentId = payment
            order.save(commit=False, change_type='update')

        delivery = request.POST.get('delivery')
        if str(order.DeliveryId) != delivery:
            order.DeliveryId = delivery
            order.save(commit=False, change_type='update')
            delivery_changed = True

        if delivery_changed and delivery == '1':
            order.AddressId = None
            order.save(commit=False, change_type='update')
        else:
            if request.POST.get('change_address'):
                new_address = Address(
                    street=request.POST.get('street'),
                    house_number=request.POST.get('house_number'),
                    flat_number=request.POST.get('flat_number'),
                    postal_code=request.POST.get('postal_code'),
                    city=request.POST.get('city'),
                    country=request.POST.get('country'),
                    contractor_id=contractor_id,
                )
                new_address.save(commit=False)
                order.AddressId = new_address.Id
                order.save(commit=False, change_type='update')
            else:
                address = request.POST.get('address')
                if str(order.AddressId) != address:
                    order.AddressId = address
                    order.save(commit=False, change_type='update')


        # update pozycji
        valid_pos_products_ids = []
        for act_pos in actual_positions:
            for upd_pos in updated_positions:
                if str(act_pos.ProductId) == upd_pos['product']:
                    if str(act_pos.Quantity) != upd_pos['quantity']:
                        session = SESSION()
                        position_to_update = session.query(OrderPosition).filter(
                            OrderPosition.Id == act_pos.Id
                        ).one()
                        session.close()
                        position_to_update.Quantity = upd_pos['quantity']
                        position_to_update.save(commit=False)
                    valid_pos_products_ids.append(upd_pos['product'])

        # usunięte pozycje
        for act_pos in actual_positions:
            if str(act_pos.ProductId) not in valid_pos_products_ids:
                session = SESSION()
                position_to_remove = session.query(OrderPosition).filter(
                    OrderPosition.Id == act_pos.Id
                ).one()
                session.close()
                position_to_remove.OrderId = 0
                position_to_remove.save(commit=False)

        # nowe pozycje
        for upd_pos in updated_positions:
            if upd_pos['product'] not in valid_pos_products_ids:
                OrderPosition(
                    order_id=order_id,
                    product_id=upd_pos['product'],
                    quantity=upd_pos['quantity'],
                ).save()

        result.update({
            'order_done': True,
            'order_success': True,
        })
        return result

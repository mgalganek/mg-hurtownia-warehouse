# -*- coding: utf-8 -*-

from pyramid.view import view_config
from pyramid.security import authenticated_userid

from hurtownia.src.helpers import (
    get_user_name,
    get_all_products,
    get_order_by,
    get_order_amount
)


@view_config(
    route_name='order_details',
    renderer='order_details.mako',
    permission='use'
)
def order_details(request):
    logged_user = authenticated_userid(request)
    user_name = get_user_name(logged_user)

    order_id = request.matchdict.get('order')
    order_details = get_order_by(order_id)
    amount = get_order_amount(order_id=order_id)
    products = get_all_products(order_id)

    return {
        'user': user_name,
        'order': order_details,
        'products': products,
        'amount': amount
    }

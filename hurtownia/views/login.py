# -*- coding: utf-8 -*-

import md5
from base64 import b64encode

from pyramid.view import view_config
from pyramid.security import remember
from pyramid.httpexceptions import HTTPFound

from hurtownia.src.models import SESSION, Contractor


@view_config(route_name='login', renderer='login.mako')
def login(request):
    u"""View for login."""
    session = SESSION()
    user_email = request.POST.get('email')
    user_password = request.POST.get('password')
    user_password = b64encode(md5.new(user_password).digest())

    if any([user_email, user_password]):
        for contractor in session.query(Contractor).all():
            if (
                user_email == contractor.Email and
                user_password == contractor.Password
            ):
                session.close()
                print contractor.Name+" is logged"
                header = remember(request, contractor.Email)
                return HTTPFound(location='/', headers=header)

    session.close()
    return HTTPFound(location='check_login')

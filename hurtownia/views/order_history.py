# -*- coding: utf-8 -*-

from pyramid.security import authenticated_userid
from pyramid.view import view_config

from hurtownia.src.helpers import (
    get_all_orders_by_contractor,
    get_user_details,
    get_user_name
)
from hurtownia.src.pages import paginagte


@view_config(
    route_name='order_history', renderer='order_history.mako',
    permission='use')
def order_history(request):
    u"""Order history"""
    logged_user = authenticated_userid(request)
    user_name = get_user_name(logged_user)
    user = get_user_details(logged_user)
    orders = get_all_orders_by_contractor(contractor_id=user.Id)

    return {
        'orders': paginagte(query=orders, request=request),
        'user': user_name
    }
